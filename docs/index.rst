.. Example project documentation master file, created by
   sphinx-quickstart on Mon Mar 22 14:17:44 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Maximum entropy distributions
=============================

.. image:: https://gitlab.com/dsbowen/maxentropy/badges/master/pipeline.svg
   :target: https://gitlab.com/dsbowen/maxentropy/-/commits/master

.. image:: https://gitlab.com/dsbowen/maxentropy/badges/master/coverage.svg
   :target: https://gitlab.com/dsbowen/maxentropy/-/commits/master

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black

.. image:: https://badge.fury.io/py/max-entropy.svg
   :target: https://badge.fury.io/py/max-entropy

.. image:: https://mybinder.org/badge_logo.svg
   :target: https://mybinder.org/v2/gl/dsbowen%2Fmaxentropy/HEAD?filepath=examples%2Fexample_notebook.ipynb

Description
-----------

Scipy-style maximum entropy distributions.

Install
-------

.. code-block:: bash

   $ pip install max-entropy

.. toctree::
   :maxdepth: 2
   :caption: Contents

   API <api>

License
-------

MIT

Citations
---------

.. code-block:: bibtex

   @software{bowen2021maxentropy,
      title={Maximum entropy distributions},
      author={Dillon Bowen},
      year={2021}
   } 
