# Maximum entropy distributions

[![pipeline status](https://gitlab.com/dsbowen/maxentropy/badges/master/pipeline.svg)](https://gitlab.com/dsbowen/maxentropy/-/commits/master)
[![coverage report](https://gitlab.com/dsbowen/maxentropy/badges/master/coverage.svg)](https://gitlab.com/dsbowen/maxentropy/-/commits/master)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dsbowen%2Fmaxentropy/HEAD?filepath=examples%2Fexample_notebook.ipynb)

This package implements scipy-style maximum entropy distributions.
