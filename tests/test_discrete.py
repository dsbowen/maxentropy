import numpy as np
from scipy.stats import bernoulli, geom

from maxentropy import maxentropy_discrete

TOL = .02

maxentropy_uniform = maxentropy_discrete([], a=0, b=100)

def test_uniform_pdf():
    x = np.arange(0, 101)
    maxentropy_pmf = maxentropy_uniform.pmf(x)
    assert (abs(maxentropy_pmf - 1/100) < TOL).mean() == 1

def test_uniform_cdf():
    x = np.arange(0, 101)
    maxentropy_cdf = maxentropy_uniform.cdf(x)
    assert abs((maxentropy_cdf - np.cumsum(np.ones(101)/100)) < TOL).mean() == 1

maxentropy_bernoulli = maxentropy_discrete([(lambda x: x, 1/3)], a=0, b=1)

def test_bernoulli_pdf():
    x = np.arange(0, 2)
    maxentropy_pmf = maxentropy_bernoulli.pmf(x)
    assert (abs(maxentropy_pmf - bernoulli(1/3).pmf(x)) < TOL).mean() == 1

def test_bernoulli_cdf():
    x = np.arange(0, 2)
    maxentropy_cdf = maxentropy_bernoulli.cdf(x)
    assert (abs(maxentropy_cdf - bernoulli(1/3).cdf(x)) < TOL).mean() == 1

maxentropy_geom = maxentropy_discrete([(lambda x: x, 2)], a=1, b=10)

def test_geom_pdf():
    x = np.arange(1, 11)
    maxentropy_pmf = maxentropy_geom.pmf(x)
    assert (abs(maxentropy_pmf - geom(.5).pmf(x)) < TOL).mean() == 1

def test_geom_cdf():
    x = np.arange(1, 11)
    maxentropy_cdf = maxentropy_geom.cdf(x)
    assert (abs(maxentropy_cdf - geom(.5).cdf(x)) < TOL).mean() == 1
