import numpy as np
from scipy.stats import bernoulli, expon, norm, uniform

from maxentropy import maxentropy_continuous, maxentropy_discrete

TOL = .02

maxentropy_uniform = maxentropy_continuous([], a=0, b=1)

def test_uniform_pdf():
    x = np.linspace(0, 1)
    maxentropy_pdf = maxentropy_uniform.pdf(x)
    assert (abs(maxentropy_pdf - uniform().pdf(x)) < TOL).mean() == 1

def test_uniform_cdf():
    x = np.linspace(0, 1)
    maxentropy_cdf = maxentropy_uniform.cdf(x)
    assert (abs(maxentropy_cdf - uniform().cdf(x)) < TOL).mean() == 1

maxentropy_expon = maxentropy_continuous([(lambda x: x, 1)], a=0, b=10)

def test_expon_pdf():
    x = np.linspace(0, 10)
    maxentropy_pdf = maxentropy_expon.pdf(x)
    assert (abs(maxentropy_pdf - expon().pdf(x)) < TOL).mean() == 1

def test_expon_cdf():
    x = np.linspace(0, 10)
    maxentropy_cdf = maxentropy_expon.cdf(x)
    assert (abs(maxentropy_cdf - expon().cdf(x)) < TOL).mean() == 1

maxentropy_normal = maxentropy_continuous(
    [(lambda x: x, 0), (lambda x: x**2 - 0, 1)], a=-3, b=3,
)

def test_normal_pdf():
    x = np.linspace(-3, 3)
    maxentropy_pdf = maxentropy_normal.pdf(x)
    assert (abs(maxentropy_pdf - norm().pdf(x)) < TOL).mean() == 1

def test_normal_cdf():
    x = np.linspace(-3, 3)
    maxentropy_cdf = maxentropy_normal.cdf(x)
    assert (abs(maxentropy_cdf - norm().cdf(x)) < TOL).mean() == 1
